#!/bin/sh

echo DEBUG=0 >> .env
echo SQL_ENGINE=django.db.backends.postgresql >> .env
echo DATABASE=postgres >> .env

echo SECRET_KEY=$SECRET_KEY >> .env
echo DB_NAME=$DB_NAME >> .env
echo DB_USER=$DB_USER >> .env
echo DB_PASSWORD=$DB_PASSWORD >> .env
echo DB_HOST=$DB_HOST >> .env
echo DB_PORT=$DB_PORT >> .env

echo WEB_IMAGE=$IMAGE:web  >> .env
echo NGINX_IMAGE=$IMAGE:nginx  >> .env
echo CI_REGISTRY_USER=$CI_REGISTRY_USER   >> .env
echo CI_JOB_TOKEN=$CI_JOB_TOKEN  >> .env
echo CI_REGISTRY=$CI_REGISTRY  >> .env
echo IMAGE=$CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME >> .env


echo EMAIL_BACKEND=django.core.mail.backends.smtp.EmailBackend >> .env
echo EMAIL_HOST=$EMAIL_HOST >> .env
echo EMAIL_USE_TLS=True >> .env
echo EMAIL_PORT=$EMAIL_PORT >> .env
echo EMAIL_HOST_USER=$EMAIL_HOST_USER >> .env
echo EMAIL_HOST_PASSWORD=$EMAIL_HOST_PASSWORD >> .env