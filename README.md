# A Project Management Applicatiom

This repository is for a Project Management Application, it uses custom 
user authentication which enables users to sign up to an organization 
and email confirmation implementation to suit the unique signup workflow that
the system requires.


## Getting started

Steps:

1. Clone/pull/download this repository
- You'll need to have virtual enviroment installed on your machine  

    ```python
  pip3 install virtualenv
  
    ```


- Setup virtual environment

    ```markdown
    virtualenv -p python3 .virtualenv
    
    ```

    

- Activate virtual environment

    ```markdown
    source .virtualenv/bin/activate
    
    ```

    
    

   - Install requirements
    
        ```bash
        pip install -r requirements.txt
        ```



### Run migrations before starting the django-server

```python
   python manage.py migrate
```


